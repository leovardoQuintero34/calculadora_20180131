angular.module("myApp", [])
.component("calculadora",{
  templateUrl: "calculadora.html",

  controller: function () {




    function digito(val){
     if(this.entrada == 0){
      this.entrada = val;
    }else{
      this.entrada += ""+val;
    }

  }

  function punto(){
    var aux = this.entrada+"";
    if(aux.indexOf(".") < 0){
      this.entrada = this.entrada + ".";
    }
  }

  function operacion(op){

    if(this.opSaved == "0"){
      this.opSaved = op;
      this.valAnt = this.entrada;
      this.entrada = "0";
    }else{
      if(op == "="){
        this.entrada = calcula(this.valAnt,this.entrada,this.opSaved);
        this.opSaved = "0";
        this.valAnt = "0";
      }else{
        this.valAnt= calcula(this.valAnt,this.entrada,this.opSaved);
        this.entrada = "0";
        this.opSaved = op;
      }
      

    }

  }

  function calcula(valor1,valor2,signo){
    if(signo == "+"){
      return Number(valor1)+Number(valor2);
    }else if(signo == "-"){
      return Number(valor1)-Number(valor2);
    }else if(signo == "*"){
      return Number(valor1)*Number(valor2);
    }else if(signo == "/"){
      if(valor2 != "0"){
        return Number(valor1)/Number(valor2);
      }
    }
  }

  function cambiaSigno(){
    this.entrada = calcula(this.entrada,"-1","*");
  }

  function limpia(){
    this.entrada = "0";
  }

  function borra(){
    this.entrada = "0";
    this.valAnt = "0";
    this.opSaved = "0";
    this.memoria = "0";
  }

  function back(){
    if(this.entrada.length == 1){
      this.entrada = "0";
    }else{
      this.entrada = this.entrada.substring(0,this.entrada.length - 1);
    }
  }
  function memfunc(val){

    if(val == "MS"){
      this.memoria = this.entrada;
    }else if(val == "MR"){
      this.entrada = this.memoria;
    }else if(val == "MC"){
      this.memoria = 0;
    }else if(val == "M+"){
      this.memoria = calcula(this.memoria,this.entrada,"+");
    }else if(val == "M-"){
      this.memoria = calcula(this.memoria,this.entrada,"-");
    }
    console.log(this.memoria);
  }



    this.entrada = "0";
    this.valAnt = "0";
    this.opSaved = "0";
    this.memoria = "0";


  
    this.digito = digito;
    this.punto = punto;
    this.operacion = operacion;
    this.cambiaSigno = cambiaSigno;
    this.limpia = limpia;
    this.borra = borra;
    this.back = back;
    this.memfunc = memfunc;
  }
});